<?php
/**
 * @file
 * Render fbml tag for user's picture.
 */

class fb_views_handler_profile_pic extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['fbu'] = 'fbu';
  }
  
  function option_definition() {
    
    $options = parent::option_definition();
    
    $options['size'] = array('default' => '');
    $options['linked'] = array('default' => '');
    $options['width'] = array('default' => '50');
    $options['height'] = array('default' => '50');

    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    
    parent::options_form($form, $form_state);

    $form['size'] = array(
      '#type' => 'select',
      '#title' => t('Size'),
      '#default_value' => $this->options['size'],
      '#options' => array(
        'thumb' => 'thumb (50px wide)',
        'small' => 'small (100px wide)',
        'normal' => 'normal (200px wide)',
        'square' => 'square (50px by 50px)',
      ),
    );
    
    $form['linked'] = array(
      '#type' => 'select',
      '#title' => t('Link to Facebook profile'),
      '#default_value' => $this->options['linked'],
      '#options' => array(
        'no' => 'no',
        'yes' => 'yes',
      ),
    );
    
    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $this->options['width'],
    );
    
    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#default_value' => $this->options['height'],
    );
  }

  function render($values) {
    if ($fbu = $values->{$this->field_alias}) {
      return '<div class="picture"><fb:profile-pic uid="' . $fbu . '" linked="' . $this->options['linked'] . '" size="' . $this->options['size'] . '" width="' . $this->options['width'] . '" height="' . $this->options['height'] . '" /></div>';
    }
  }
}
